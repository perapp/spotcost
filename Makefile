SHELL := bash
PY := .venv/bin/python
export PREFIX ?= /usr/local
export PIPX_HOME := ${PREFIX}/lib/pipx
export PIPX_BIN_DIR := ${PREFIX}/bin

all: .venv dist

dist:
	${PY} -m flit build --no-use-vcs

run: all
	. .env && ${PY} -m spotcost data
upp: all
	. .env && ${PY} -m spotcost data/*.xlsx

.venv: requirements.txt
	python3 -m venv .venv
	${PY} -m pip install --upgrade pip
	${PY} -m pip install -r requirements.txt
	touch .venv

install:
	${PY} -m pipx install --force dist/spotcost-0.1.0-py2.py3-none-any.whl

uninstall:
	${PY} -m pipx uninstall .

clean:
	rm -rf dist

veryclean: clean
	rm -rf .venv
