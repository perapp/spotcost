#!/bin/bash
#
# Shebang utility script to launch scripts written in Python.
# To use it, chmod +x your Python script and put the following two lines at the top:
#
# #!/bin/bash
# "exec" "$(dirname $0)/../python.sh" "$0" "$@"
#
# When running your file this will:
#  - Setup a new virtual environment (if not already exist) and update pip in it
#  - pip install requirements.txt
#  - activate environment and run your script
#
set -euo pipefail
PYTHON=python3.10

if ! $(type "$PYTHON" &>/dev/null); then
    echo "$0: ERROR This application requires $PYTHON to run." >&2
    echo "Find package providing it with 'yum whatprovides $PYTHON' and install it" >&2
    exit 1
fi

PROJ_HOME="$(realpath "$(dirname "$0")/..")"
PROJ_VIRTUAL_ENV_LOCATION="$PROJ_HOME/.venv"
REQUIREMENTS="${PROJ_HOME}/requirements.txt"

function clean_venv_and_exit {
    exit_code=$?
    rm -rf "$PROJ_VIRTUAL_ENV_LOCATION"
    exit $exit_code
}

if [[ "${VIRTUAL_ENV:-}" != "$PROJ_VIRTUAL_ENV_LOCATION" ]]; then
    if [[ ! -d "$PROJ_VIRTUAL_ENV_LOCATION" ]]; then
	echo "Setting up Python venv in ${PROJ_VIRTUAL_ENV_LOCATION} ..." >&2
	"$PYTHON" -m venv "$PROJ_VIRTUAL_ENV_LOCATION"
	. "${PROJ_VIRTUAL_ENV_LOCATION}/bin/activate"
	PYTHON="${VIRTUAL_ENV}/bin/python"
	"$PYTHON" -m pip install --upgrade pip || clean_venv_and_exit
	"$PYTHON" -m pip install -r "${REQUIREMENTS}" || clean_venv_and_exit
    fi
fi

. "${PROJ_VIRTUAL_ENV_LOCATION}/bin/activate"
PYTHON="${VIRTUAL_ENV}/bin/python"

if [[ -z $(find "${PROJ_VIRTUAL_ENV_LOCATION}" -newer "${REQUIREMENTS}" -print -quit) ]]; then
    echo "${REQUIREMENTS} newer than virtual environment, rerunning pip install..." >&2
    "${PYTHON}" -m pip install --upgrade pip
    "${PYTHON}" -m pip install -r "${REQUIREMENTS}" && touch "${PROJ_VIRTUAL_ENV_LOCATION}" || exit $?
fi

if [ -e "${PROJ_HOME}/.env" ]; then
    set -a
    . "${PROJ_HOME}/.env"
fi
exec "${PYTHON}" "$@"
