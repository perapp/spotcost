# spotcost

Calculate monthly cost for electricity based on	power meters and hourly spot price. Output result as an Excel report.

Primary use case is to measure energy consumption used by multiple tenants in a single home sharing a single subscription. Assumes Swedish households using Tibber and Shelly energy meters.

Data is fetched from:
- Spot price from Nord Pool
- Total energy price from Tibber
- Network cost hard coded values for Göteborg Energi 2022.
- Energy consumption from [Shelly Cloud](https://shelly.cloud/).

## Usage

git clone project and create a .env file containing:

```
TIBBER_AUTHORIZATION_TOKEN=xyzxyzxyzxyzxyz
SHELLY_ATHORIZATION_TOKEN=xyzxyzxyzxyz
SHELLY_API_URL=https://shelly-48-eu.shelly.cloud
```

Then run spotcost/main.py

## TODO

- [ ] Tests and sample output
- [ ] Refactor into modules for report generation and data fetching
- [ ] Speed up data fetch using asynch io
- [ ] Improve Excel template with monthly overview page
- [ ] Also generate PDF report
- [ ] Schedule runs using Gitlab CI and publish reports on Gitlab pages for tentat access
