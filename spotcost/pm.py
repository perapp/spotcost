import minimalmodbus
import datetime
import retry

register_names = {
    0: "Volts_L1",
    2: "Volts_L2",
    4: "Volts_L3",
    6: "Current_L1",
    8: "Current_L2",
    10: "Current_L3",
    12: "Active_Power_L1",
    14: "Active_Power_L2",
    16: "Active_Power_L3",
    18: "Apparent_Power_L1",
    20: "Apparent_Power_L2",
    22: "Apparent_Power_L3",
    24: "Reactive_Power_L1",
    26: "Reactive_Power_L2",
    28: "Reactive_Power_L3",
    30: "Power_Factor_L1",
    32: "Power_Factor_L2",
    34: "Power_Factor_L3",
    36: "Phase_Angle_L1",
    38: "Phase_Angle_L2",
    40: "Phase_Angle_L3",
    42: "Average_line_to_neutral_volts",
    46: "Average_line_current",
    48: "Sum_of_line_current",
    52: "Total_System_Active_Power",
    56: "Total_System_Apparent_Power",
    60: "Total_System_Reactive_Power",
    62: "Total_System_Power_Factor",
    66: "Total_System_Phase_Angle",
    70: "Frequency_Of_Supply_Voltages",
    72: "Import_Wh_since_last_reset",
    74: "Export_Wh_since_last_reset",
    76: "Import_VArh_since_last_reset",
    78: "Export_VArh_since_last_reset",
    80: "VAh_since_last_reset",
    82: "Ah_since_last_reset",
    84: "Total_system_power_demand",
    86: "Maximum_total_system_power_demand",
    100: "Total_system_VA_demand",
    102: "Maximum_total_system_VA_demand",
    104: "Neutral_current_demand",
    106: "Maximum_neutral_current_demand",
    200: "Line_1_to_Line_2_volts",
    202: "Line_2_to_Line_3_volts",
    204: "Line_3_to_Line_1_volts",
    206: "Average_line_to_line_volts",
    224: "Neutral_current",
    234: "Phase_1_LN_volts_THD",
    236: "Phase_2_LN_volts_THD",
    238: "Phase_3_LN_volts_THD",
    240: "Phase_1_Current_THD",
    242: "Phase_2_Current_THD",
    244: "Phase_3_Current_THD",
    248: "Average_line_to_neutral_volts_THD",
    250: "Average_line_current_THD",
    254: "Total_system_power_factor",
    258: "Phase_1_current_demand",
    260: "Phase_2_current_demand",
    262: "Phase_3_current_demand",
    264: "Maximum_phase_1_current_demand",
    266: "Maximum_phase_2_current_demand",
    268: "Maximum_phase_3_current_demand",
    334: "Line_1_to_line_2_volts_THD",
    336: "Line_2_to_line_3_volts_THD",
    338: "Line_3_to_line_1_volts_THD",
    340: "Average_line_to_line_volts_THD",
    342: "Total_kwh",
    344: "Total_kvarh",
    346: "L1_import_kwh",
    348: "L2_import_kwh",
    350: "L3_import_kwh",
    352: "L1_export_kwh",
    354: "L2_export_kwh",
    356: "L3_export_kwh",
    358: "L1_total_kwh",
    360: "L2_total_kwh",
    362: "L3_total_kwh",
    364: "L1_import_kvarh",
    366: "L2_import_kvarh",
    368: "L3_import_kvarh",
    370: "L1_export_kvarh",
    372: "L2_export_kvarh",
    374: "L3_export_kvarh",
    376: "L1_total_kvarh",
    378: "L2_total_kvarh",
    380: "L3_total_kvarh",
}

@retry.retry(tries=5, delay=1, backoff=2)
def get_data():
    instrument = connect()

    data = []
    for reg_id in (0x0034, 0x0156):
        data.append(instrument.read_float(registeraddress=reg_id,
                                          functioncode=4,
                                          number_of_registers=2))
    data.insert(0, datetime.datetime.now().astimezone())
    instrument.serial.close()
    return data

def connect():
    instrument = minimalmodbus.Instrument('/dev/ttyUSB0', 1)
    instrument.serial.baudrate = 9600
    instrument.serial.bytesize = 8
    instrument.serial.parity = minimalmodbus.serial.PARITY_NONE
    instrument.serial.stopbits = 1
    instrument.serial.timeout  = 0.5
    instrument.mode = minimalmodbus.MODE_RTU
    instrument.clear_buffers_before_each_transaction = True
    instrument.close_port_after_each_call = True

    return instrument
