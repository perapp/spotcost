import datetime
import nordpool.elspot
import pytz
import gql
import gql.transport.aiohttp
import base64
from pprint import pprint
import box
import os

# data from https://www.goteborgenergi.se/privat/elnat/elnatsavgiften#prisvilla
net_vendor_price_per_year = {
    2022: 0.2075/1.25,
    2023: 0.2550/1.25,
    2024: 0.2550/1.25,
    2025: 0.2500/1.25,
}
net_tax_per_year = {
    2022: 0.450/1.25,
    2023: 0.490/1.25,
    2024: 0.535/1.25,
    2025: 0.54875/1.25,
}
htz = pytz.timezone("Europe/Stockholm")

def get_price_data(period_start, period_end):
    # TODO: Rety and return empty if fail
    
    if period_start == None or period_end == None:
        return {
            "power_spot_price": None,
            "power_vendor_price": None,
            "power_vat": None,
            "net_vendor_price": None,
            "net_tax": None,
            "net_vat": None}

    time = period_start.replace(minute=0, second=0, microsecond=0)
    power_spot_price = get_nordpool_spot_price(time)
    power_vendor_price, power_vat = get_tibber_price(time)
    power_vendor_price = power_vendor_price - power_spot_price  # tibber price include spot price

    year = time.astimezone(htz).year
    net_vendor_price = net_vendor_price_per_year[year]
    net_tax = net_tax_per_year[year]
    net_vat = (net_vendor_price + net_tax) * 0.25

    return {
        "power_spot_price": power_spot_price,
        "power_vendor_price": power_vendor_price,
        "power_vat": power_vat,
        "net_vendor_price": net_vendor_price,
        "net_tax": net_tax,
        "net_vat": net_vat}

_nordpool_cache = {}
def get_nordpool_spot_price(time):
    global _nordpool_cache
    if time in _nordpool_cache:
        return _nordpool_cache[time]

    date = time.astimezone(pytz.timezone("Europe/Stockholm")).date()
    spot_prices = nordpool.elspot.Prices(currency="SEK").hourly(areas=["SE3"], end_date=date)
    for value in spot_prices["areas"]["SE3"]["values"]:
        _nordpool_cache[value["start"]] = value["value"] / 1000.0

    if time in _nordpool_cache:
        return _nordpool_cache[time]
    raise Exception(f"Nordpool result period ({min(_nordpool_cache.keys())},{max(_nordpool_cache.keys())}) does not contain requested time {time}")

_tibber_cache = {}
def get_tibber_price(time):
    global _tibber_cache
    if time in _tibber_cache:
        return _tibber_cache[time]

    transport = gql.transport.aiohttp.AIOHTTPTransport(url="https://api.tibber.com/v1-beta/gql",
                                                       headers={'Authorization': os.environ["TIBBER_AUTHORIZATION_TOKEN"]})
    client = gql.Client(transport=transport, fetch_schema_from_transport=True)
    time_b64 = base64.b64encode(bytes(str(time), "UTF-8")).decode("ascii")
    date = time.date() - datetime.timedelta(days=1)
    date_b64 = base64.b64encode(bytes(str(date), "UTF-8")).decode("ascii")
    #print(f"Query tibber for price at {time}")
    query = f"""
                {{
                  viewer {{
                    homes {{
                      currentSubscription{{
                        priceInfo {{
                          range(resolution: HOURLY, after: "{date_b64}", first: 72) {{
                            nodes {{
                              startsAt
                              energy
                              tax
                            }}
                          }}
                          today {{
                              startsAt
                              energy
                              tax
                          }}
                          tomorrow {{
                              startsAt
                              energy
                              tax
                          }}
                        }}
                      }}
                    }}
                  }}
                }}
    """
    #print(query)
    result = client.execute(gql.gql(query))
    #pprint(result)
    result = box.Box(result)
    price_info = result.viewer.homes[0].currentSubscription.priceInfo
    nodes = price_info.range.nodes + price_info.today + price_info.tomorrow
    for node in nodes:
        start = datetime.datetime.fromisoformat(node.startsAt)
        _tibber_cache[start] = (node["energy"], node["tax"])

    if time in _tibber_cache:
        return _tibber_cache[time]
    for x in sorted(_tibber_cache.keys()):
        print(x)
    raise Exception(f"Tibber result period ({min(_tibber_cache.keys())},{max(_tibber_cache.keys())}) does not contain requested time {time}")
