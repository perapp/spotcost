"""
spotcost descr....
"""
__version__ = "0.1.0"

import itertools
import datetime
import zoneinfo
import os
import base64
import urllib.request
import json
import pathlib
import shutil
import gql
import gql.transport.aiohttp
import openpyxl
import nordpool.elspot
from dateutil.relativedelta import relativedelta
import time
import sys
import pytz

from . import pm
from . import price

OUTPUT_DATE_FMT = "%Y-%m-%d %H:%M:%S"
htz = pytz.timezone("Europe/Stockholm")

"""
start,end,start_total,end_total,consumption,spot_cost_avg
"""

def write_pm_data(data_dir):
  try:
    (end, power, end_total) = pm.get_data()
  except:
    return

  for wb in get_wbs_for_time(data_dir, end):
    ws = wb["data"]
    ci = get_column_indices(ws)

    # copy last row as template for new one
    row = duplicate_last_row(ws)

    # set start values from previous end values
    row[ci["start"]].value = row[ci["end"]].value
    row[ci["start_total"]].value = row[ci["end_total"]].value
    start = parse_time(row[ci["start"]].value)

    row[ci["end"]].value = end.astimezone(datetime.timezone.utc).replace(tzinfo=None)
    row[ci["start_h"]].value = start.astimezone(htz).replace(tzinfo=None) if start else None
    row[ci["end_h"]].value = end.astimezone(htz).replace(tzinfo=None) if end else None

    # set pm values
    row[ci["end_power"]].value = power
    row[ci["end_total"]].value = end_total

    # intermediate save if price calc fails
    wb.save(wb.file)

    # set price values
    price_data = price.get_price_data(start, end)
    for x, y in price_data.items():
      row[ci[x]].value = y

    wb.save(wb.file)

def update_price_data(wb_file):
  wb = openpyxl.load_workbook(filename = wb_file)
  ws = wb["data"]
  ci = get_column_indices(ws)
  for row in ws.iter_rows(min_row=2):
    start = parse_time(row[ci["start"]].value)
    end = parse_time(row[ci["end"]].value)
    price_data = price.get_price_data(start, end)
    print(start, " ".join([x + "=" + format(y,'8.5f') if y is not None else "None"
                           for x, y in price_data.items()]))
    for x, y in price_data.items():
      row[ci[x]].value = y
  wb.save(wb_file)
    

def get_column_indices(ws):
  header_row = list(ws.iter_rows(min_row=1, max_row=1))[0]

  # column indices, a dict with column names as keys and column numbers as values
  ci = {x.value.split(" ")[0]: i
        for i, x in enumerate(header_row, start=0)
        if x.value}
  return ci
  

def duplicate_last_row(ws):
  """
  Copy last row, translating formulas, and return the new last row
  """
  i = ws.max_row  # row to copy from
  j = i+1         # row to copy to
  # to translate functions, actually move row i to j and then copy j->i

  # make copy of old values before moving
  values = list(ws.iter_rows(min_row=i, max_row=i, values_only=True))[0]
  ws.move_range(f"A{i}:ZZ{i}", rows=1, translate=True)

  # copy values
  for xi, xv in zip(list(ws.iter_rows(min_row=i, max_row=i))[0], values):
    xi.value = xv

  # copy formatting j->i
  for xi, xj in zip(list(ws.iter_rows(min_row=i, max_row=i))[0], list(ws.iter_rows(min_row=j, max_row=j))[0]):
    xi.number_format = xj.number_format

  return list(ws.iter_rows(min_row=j, max_row=j))[0]

def get_wbs_for_time(data_dir, time: datetime.datetime):
  periods = set([get_billing_period_for_time(time),
                 get_billing_period_for_time(time - relativedelta(days=1)),
                 get_billing_period_for_time(time + relativedelta(days=1))])

  for (start, end) in periods:
    wbfile = data_dir/f"spotcost_{start:%Y-%m}.xlsx"
    if not wbfile.exists():
      wbfile.parent.mkdir(parents=True, exist_ok=True)
      shutil.copy(pathlib.Path(__file__).parent/"template.xlsx", wbfile)

    wb = openpyxl.load_workbook(filename = wbfile)
    wb.file = wbfile
    ws = wb["overview"]
    ws["B2"] = start.astimezone(datetime.timezone.utc).replace(tzinfo=None)
    ws["C2"] = end.astimezone(datetime.timezone.utc).replace(tzinfo=None)

    yield wb

def get_wbfile_for_time(data_dir, time: datetime.datetime):
  return data_dir/f"spotcost_{time:%Y-%m}.xlsx"

def get_billing_period_for_time(time):
  start = time.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
  end = start + relativedelta(months=1)
  return (start, end)

def format_time(x):
  if x is None:
    return None
  if not isinstance(x, datetime.datetime):
    x = datetime.datetime.fromisoformat(str(x))
  return x.astimezone(datetime.timezone.utc).strftime(OUTPUT_DATE_FMT)

def parse_time(x):
  if x is None:
    return None
  elif isinstance(x, datetime.datetime):
    return x.replace(tzinfo=datetime.timezone.utc)
  else:
    return datetime.datetime.strptime(x, OUTPUT_DATE_FMT).replace(tzinfo=datetime.timezone.utc)

def main():
  if len(sys.argv) > 1:
    data_dir = pathlib.Path(sys.argv[1])
  else:
    data_dir = pathlib.Path(".")
  if data_dir.is_file():
    update_price_data(data_dir)
  else:
    write_pm_data(data_dir)
    
if __name__ == "__main__":
    main()
    
